# 心链openapi

# 产品介绍
心链平台提供专业的数字权益商品标准化接口和免费接入服务，数字权益商品涵盖话费、流量、音乐会员、视频会员、抖钻、加油卡等多种品类，可满足使用者多方面的业务需求，丰富企业的产品内容、提升竞争优势。


# 产品功能
- **商品管理**：提供API商户可以进行对接的商品类目和编号。
- **订单管理**：订单明细查询、充值记录核对等。
- **应用配置**：密钥管理、账号绑定、IP白名单添加、参数配置。

# 使用场景
拥有一定技术团队的企业，接入充值API，用于手机App、PC客户端、微信公众号、小程序、支付宝生活号等多种场景，为自己的用户提供在线充值服务，丰富服务内容，促进流量变现及增强用户活跃度。

# 产品优势
- **接口稳定性高，充值到账快**：专业技术团队保证接口安全、稳定，用户从下单到充值成功率高，到账时间短。
- **货源丰富，涵盖全品类**：各种数字权益商品，全品类覆盖，可满足不同行业、不同场景需求。
- **稳定、高效的解决方案**：专业技术团队打造，稳定高效，畅享低延迟高稳定解决方案，立志于为企业带来变革级全新体验，支持各种技术方案、多维度全链路场景，为企业降本增效。
- **卓越的售后服务**：标准的客服服务体系，成熟的售前售后服务，24小时在线第一时间解决客诉，消除负面信息，为客户提供全方面的服务。



#### composer安装
````
composer require xinlian/openapi
````
```
仓库地址：https://gitee.com/ylSuperman/xinlian-openapi.git
```

````
//接入文档地址https://doc.baiopen.com
//调用方式
$config = [
    "access_token"          => "您的token",
    "sign_key"          => "您的签名key",
];
$xinlian = new \XinLian\OpenApi($config);
//查询余额
$response = $xinlian->queryBalance();
//返回结果是一个ResponseInterface对象，详细参数请查看guzzle官方文档
$body = $response->getBody()->getContents();

提供一下函数调用
//查询地区
$response = $xinlian->queryArea();
//创建订单
$response = $xinlian->createOrder();
//查询订单
$response = $xinlian->queryOrder();
//查询手机号归属地
$response = $xinlian->queryRegion();
//取消订单
$response = $xinlian->cancelOrder();
//查询余额
$response = $xinlian->queryBalance();
//查询类目
$response = $xinlian->queryCategory();
//查询商品
$response = $xinlian->queryProduct();
//查询商品sku
$response = $xinlian->querySku();
````