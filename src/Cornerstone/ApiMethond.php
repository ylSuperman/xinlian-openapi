<?php
namespace XinLian\Cornerstone;

class ApiMethond
{
    /**
     * 查询手机号归属地
     */
    const QUERY_REGION = 'query.region';
    /**
     * 查询地区对照表
     */
    const QUERY_AREA  = 'query.area';
    /**
     * 查询余额
     */
    const QUERY_BALANCE = 'query.balance';
    /**
     * 创建订单
     */
    const CREATE_ORDER = 'create.order';
    /**
     * 取消订单
     */
    const CANCEL_ORDER = 'cancel.order';
    /**
     * 查询订单
     */
    const QUERY_ORDER = 'query.order';
    /**
     * 查询订单列表
     */
    const QUERY_ORDER_LIST = 'query.order.list';
    /**
     * 查询类目
     */
    const QUERY_CATEGORY = 'query.category';
    /**
     * 查询商品
     */
    const QUERY_PRODUCT = 'query.product';
    /**
     * 查询sku
     */
    const QUERY_SKU = 'query.sku';
}