<?php
namespace XinLian;


use XinLian\Cornerstone\ApiMethond;

class OpenApi
{
    //默认配置
    protected $config = [
        "access_token"          => "您的访问token",
        "sign_key"          => "您的签名key",
        'url'      =>'https://m.baiopen.com/openapi/method',
    ];

    /**
     * XinLian constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = array_merge($this->config, $config);
    }

    /**
     * 创建订单
     * @param string $merchant_no 商户订单号 （最大长度32个字符）
     * @param string $code 商品sku编码
     * @param array $format 充值账号格式
     * @param string $notice_url 异步通知地址
     * @param string $uid 自定义用户id
     * @param string $format_type 充值类型
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @author yanglei
     * @Date 2024/3/27
     */
    public function createOrder(string $merchant_no,string $code,array $format,string $format_type="common",string $notice_url="",string $uid=""){
        $data = [
            "merchant_no"=>$merchant_no,
            "sku_code"=>$code,
            "format_type"=>$format_type,
            "format"=>$format,
            "notice_url"=>$notice_url,
            "uid"=>$uid,
        ];
        return $this->method(ApiMethond::CREATE_ORDER,$data);
    }

    /**
     * 订单查询
     * @param string $order_no
     * @param string $platform_no
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @author yanglei
     * @Date 2024/3/27
     */
    public function queryOrder(string $merchant_no,string $order_no=""){
        $data = [
            "merchant_no"=>$merchant_no,
            "order_no"=>$order_no,
        ];
        return $this->method(ApiMethond::QUERY_ORDER,$data);
    }

    /**
     * 查询手机号归属地
     * @param string $phone
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @author yanglei
     * @Date 2024/3/27
     */
    public function queryRegion(string $phone){
        $data = [
            "phone"=>$phone
        ];
        return $this->method(ApiMethond::QUERY_REGION,$data);
    }

    /**
     * 查询地区对照表
     * @param $pid
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @author yanglei
     * @Date 2024/3/27
     */
    public function queryArea($pid=0){
        $data = [
            "pid"=>$pid
        ];
        return $this->method(ApiMethond::QUERY_AREA,$data);
    }

    /**
     * 查询账户余额
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @author yanglei
     * @Date 2024/3/27
     */
    public function queryBalance(){
        return $this->method(ApiMethond::QUERY_BALANCE);
    }

    /**
     * 取消订单
     * @param $order_on
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @author yanglei
     * @Date 2024/3/27
     */
    public function cancelOrder($order_on){
        $data = [
            "order_on"=>$order_on
        ];
        return $this->method(ApiMethond::CANCEL_ORDER,$data);
    }

    /**
     * 查询分类
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @author yanglei
     * @Date 2024/3/27
     */
    public function queryCategory(){
        return $this->method(ApiMethond::QUERY_CATEGORY);
    }

    /**
     * 查询商品
     * @param int $cate_id
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @author yanglei
     * @Date 2024/3/27
     */
    public function queryProduct(int $cate_id = 0){
        $data = [
            "cate_id"=>$cate_id,
        ];
        return $this->method(ApiMethond::QUERY_PRODUCT,$data);
    }

    /**
     * 查询商品sku
     * @param int $product_id
     * @param int $cate_id
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @author yanglei
     * @Date 2024/3/27
     */
    public function querySku(int $product_id = 0,int $cate_id = 0){
        $data = [
            "product_id"=>$product_id,
            "cate_id"=>$cate_id,
        ];
        return $this->method(ApiMethond::QUERY_SKU,$data);
    }

    /**
     * 验证回调签名
     * @param array $param
     * @return bool
     */
    public function verifyNotice(array $param){
        $sign = Tool::createSign($param,$this->config['sign_key']);
        if($sign == (string)$param['sign']){
            return true;
        }
        return false;
    }
    /**
     * @introduce 请求接口
     * @param string $method 接口名称
     * @param json $data 参数
     * @param string $attach 透传参数
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @author yanglei
     * @Date 2024/3/27
     */
    public function method($method,$data = [],$attach = ''){
        $param = [
            'access_token'        =>  $this->config['access_token'],
            'once'        =>  uniqid() ,
            'timestamp'  =>  time()*1000,
            'attach'       =>  $attach,
            'format'     =>  'JSON',
            'sign_type'    =>  'MD5',
            'version'       =>  '1.0.0',
            'method'       =>  $method,
            'data'       =>  json_encode($data,256),
        ];
        $param = array_merge($param,['sign'=>Tool::createSign($param,$this->config['sign_key'])]);
        return Tool::http($this->config['url'],$param);
    }
}